#include "../includes/ll_parser.h"
#include "../includes/lexer.h"
#include "../includes/ast.h"


static int node_id = 4000;

int parse_redirection(struct data_struct *data, struct redirection *pStruct)//check _next_word sur ionnumber
{

    pStruct->id = node_id++;
    pStruct->type = e_redirection;
    char next_char = check_next_char(data);
    pStruct->ionumber.token = -1;
    if ((next_char != '<') && (next_char != '>'))
    {
        if ((next_char < '0') || (next_char > '9'))
            return 1;
        pStruct->ionumber.token = next_char - '0';
    }
    char* comparator = eat_comparator(data);
    pStruct->operator.token = strdup(comparator);
    if ((!strcmp(comparator, "<<")) || (!strcmp(comparator, "<<-")))
        strcpy(pStruct->rest.heredoc.token, check_here_doc(data));//FIXME m_leak
    else if ((!strcmp(comparator, "<")) || (!strcmp(comparator, ">"))
             || (!strcmp(comparator, "<<")) || (!strcmp(comparator, ">>"))
             || (!strcmp(comparator, "<&")) || (!strcmp(comparator, ">&"))
             || (!strcmp(comparator, "<>")))
        pStruct->rest.word.token = strdup(check_here_doc(data));//FIXME m_leak
    else
        return 1;
    return 0;
}

int parse_prefix(struct data_struct *data, struct prefix *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_prefix;
    int index_save = data->index;
    if(parse_redirection(data, &pStruct->prefix_u.redirection) == 0)
    {
        pStruct->type = e_redirection;
        return 0;
    }
    pStruct->type = e_assign_word;
    data->index = index_save;
    char *assignment_word = eat_next_word(data);
    pStruct->prefix_u.assignment_word.token = strdup(assignment_word);
    return 0;
}

int parse_element(struct data_struct *data, struct element *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_element;
    int index_save = data->index;
    if(parse_redirection(data, &pStruct->element_u.redirection) == 0)
    {
        pStruct->type = e_redirection;
        return 0;
    }
    pStruct->type = e_word;
    data->index = index_save;
    char *word = eat_next_word(data);
    pStruct->element_u.word.token = strdup(word);
    return 0;
}

static int _recursive_compound_list(struct data_struct *data, struct compound_list *pStruct)
{
    // _e_o_expression
    //      ou
    //('&'|';'|'\n') ('\n')*
    //      ou
    //((';'|'&'|'\n') ('\n')* and_or)+ [('&'|';'|'\n') ('\n')*]

    char *next_word = check_next_word(data);

    if((!strcmp(next_word, "&"))
       || (!strcmp(next_word, ";"))
       || (!strcmp(next_word, "\n")))
        data->index++;
    else
        return 0;

    while (!strcmp(check_next_word(data), "\n"))
        data->index++;

    // _e_o_expression
    //      ou
    //and_or [('&'|';'|'\n') ('\n')*]
    //      ou
    // and_or((';'|'&'|'\n') ('\n')* and_or)* [('&'|';'|'\n') ('\n')*]

    int index_save = data->index;
    ++pStruct->nb_aors;
    pStruct->and_ors = realloc(pStruct->and_ors, pStruct->nb_aors * sizeof(struct and_or));
    if(parse_and_or(data, &pStruct->and_ors[pStruct->nb_aors - 1], 1) == 1)
    {
        data->index = index_save;
        return 0;
    }

    // _e_o_expression
    //      ou
    // ('&'|';'|'\n') ('\n')*
    //          ou
    // ((';'|'&'|'\n') ('\n')* and_or)+ [('&'|';'|'\n') ('\n')*]

    return _recursive_compound_list(data, pStruct);
}

int parse_compound_list(struct data_struct *data, struct compound_list *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_compound_list;
    while (!strcmp(check_next_word(data), "\n"))
        data->index++;

    pStruct->nb_aors = 0;
    pStruct->nb_ops = 0;

    pStruct->and_ors = malloc(++pStruct->nb_aors * sizeof (struct and_or));
    if(parse_and_or(data, pStruct->and_ors, 1) == 1)
        return 1;
    return _recursive_compound_list(data, pStruct);
}