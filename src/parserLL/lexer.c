#include "../includes/lexer.h"

char check_next_char(struct data_struct *pStruct)
{
    char *res = check_next_word(pStruct);
    if (res)
        return res[0];
    else
        return '\0';
}

char *eat_next_word(struct data_struct *pStruct)
{
    char *res = check_next_word(pStruct);
    if (res)
        pStruct->index++;
    return res;
}

char *check_next_word(struct data_struct *pStruct)
{
    if (pStruct->index < pStruct->nb_words)
        return pStruct->input[pStruct->index];
    return null;
}

char *check_here_doc(struct data_struct *pStruct)//FIXME
{
    pStruct = pStruct;
    return null;
}

char *eat_comparator(struct data_struct *pStruct)
{
    const char *src = pStruct->input[pStruct->index];
    size_t n = strlen(src);
    size_t i;
    char *dest = malloc(n - 1);
    for (i = 1 ; i < n && src[i] != '\0'; i++)
    {
        dest[i] = src[i];
    }
    pStruct->index++;
    return dest;
}