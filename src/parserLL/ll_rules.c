#include "../includes/ll_parser.h"
#include "../includes/lexer.h"

static int node_id = 0;

static int return_func(struct data_struct *data, struct rule_for *pStruct)
{
    pStruct->id = node_id++;
    while (!strcmp(check_next_word(data), "\n"))
        data->index++;
    return parse_do_group(data, &pStruct->do_group);
}
int parse_rule_for(struct data_struct *data, struct rule_for *pStruct)//FIXME coding style
{
    pStruct->type = e_rule_for;
    //'for' word([';']|[('\n')* 'in' (word)* (';'|'\n')]) ('\n')* do_group
    if(strcmp(eat_next_word(data), "for"))
        return 1;
    char *word = eat_next_word(data);
    pStruct->words = malloc(sizeof(struct word));
    pStruct->words[0].token = strdup(word);
    //([';']|[('\n')* 'in' (word)* (';'|'\n')]) ('\n')* do_group
    char *next_word = check_next_word(data);
    if(!strcmp(next_word, ";"))//';' ('\n')* do_group
        return return_func(data, pStruct);
    else if(!strcmp(next_word, "\n"))
    {
        //('\n')* 'in' (word)* (';'|'\n') ('\n')* do_group
        while (!strcmp(check_next_word(data), "\n"))
            data->index++;
        if(strcmp(eat_next_word(data), "in"))
            return 1;
        //(word)* (';'|'\n') ('\n')* do_group
        next_word = check_next_word(data);//FIXME si le mot commence par ";" -> pb
        if((strcmp(next_word, ";")) && (strcmp(next_word, "\n")))
        {
            //(word)+ (';'|'\n')]) ('\n')* do_group
            do
            {
                //(word)+ (';'|'\n')]) ('\n')* do_group
                word = eat_next_word(data);
                ++pStruct->nb_words;
                pStruct->words = realloc(pStruct->words,
                        pStruct->nb_words * sizeof (struct word));
                pStruct->words[pStruct->nb_words - 1].token = strdup(word);
            } while((strcmp(check_next_word(data), ";"))
                    && (strcmp(check_next_word(data), "\n")));
            //(';'|'\n') ('\n')* do_group
            data->index++;
            //('\n')* do_group
        }
        else if((!strcmp(next_word, ";")) || (!strcmp(next_word, "\n")))
            data->index++;
            //('\n')* do_group
        else
        {
            free(pStruct->words);
            return 1;
        }
        return return_func(data, pStruct);
    }
    return 1;
}
int parse_rule_until(struct data_struct *data, struct rule_until *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_rule_until;
    if(strcmp(eat_next_word(data), "until"))
        return 1;
    if(parse_compound_list(data, &pStruct->compound_list) == 1)
        return 1;
    return parse_do_group(data, &pStruct->do_group);
}

int parse_rule_while(struct data_struct *data, struct rule_while *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_rule_while;
    if(strcmp(eat_next_word(data), "while"))
        return 1;
    if(parse_compound_list(data, &pStruct->compound_list) == 1)
        return 1;
    return parse_do_group(data, &pStruct->do_group);
}

int parse_rule_case(struct data_struct *data, struct rule_case *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_rule_case;
    if(strcmp(eat_next_word(data), "case"))
        return 1;
    char *word = eat_next_word(data);
    pStruct->word.token = strdup(word);
    while (!strcmp(check_next_word(data), "\n"))
        data->index++;
    if(strcmp(eat_next_word(data), "in"))
        return 1;
    while (!strcmp(check_next_word(data), "\n"))
        data->index++;
    int index_save = data->index;
    if(parse_case_clause(data, &pStruct->case_clause) == 1)
        data->index = index_save;
    if(strcmp(eat_next_word(data), "esac"))
        return 1;
    return 0;
}

int parse_rule_if(struct data_struct *data, struct rule_if *pStruct)
{
    pStruct->type = e_rule_if;
    pStruct->id = node_id++;
    if(strcmp(eat_next_word(data), "if"))
        return 1;
    if(parse_compound_list(data, &pStruct->compound_lists[0]) == 1)
        return 1;
    if(strcmp(eat_next_word(data), "then"))
        return 1;
    if(parse_compound_list(data, &pStruct->compound_lists[1]) == 1)
        return 1;
    int index_save = data->index;
    if(parse_else_clause(data, &pStruct->else_clause) == 1)
        data->index = index_save;
    if(strcmp(eat_next_word(data), "fi"))
        return 1;
    return 0;
}
