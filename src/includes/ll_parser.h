#ifndef INC_42SH_LL_H
#define INC_42SH_LL_H

#include <stdio.h>
#include <stdlib.h>
#include "ast.h"

struct data_struct {
    char **input;
    int index;
    int nb_words;
};



int parse_input(struct data_struct *data, struct node *root);
int parse_list(struct data_struct *data, struct list *pNode, int firstRun);
int parse_and_or(struct data_struct *data, struct and_or *pNode, int firstRun);
int parse_pipeline(struct data_struct *data, struct pipeline *pNode);
int parse_command(struct data_struct *data, struct command *pCommand);
int parse_simple_command(struct data_struct *data,
                         struct simple_command *pCommand);
int parse_shell_command(struct data_struct *data,
                        struct shell_command *pCommand);
int parse_funcdec(struct data_struct *data, struct funcdec *pCommand);
int parse_redirection(struct data_struct *data, struct redirection *pStruct);
int parse_prefix(struct data_struct *data, struct prefix *pStruct);
int parse_element(struct data_struct *data, struct element *pStruct);
int parse_compound_list(struct data_struct *data, struct compound_list *pStruct);
int parse_rule_for(struct data_struct *data, struct rule_for *pStruct);
int parse_rule_while(struct data_struct *data, struct rule_while *pStruct);
int parse_rule_until(struct data_struct *data, struct rule_until *pStruct);
int parse_rule_case(struct data_struct *data, struct rule_case *pStruct);
int parse_rule_if(struct data_struct *data, struct rule_if *pStruct);
int parse_else_clause(struct data_struct *data,
                      struct else_clause *else_clause);
int parse_do_group(struct data_struct *data, struct do_group *pStruct);
int parse_case_clause(struct data_struct *data, struct case_clause *pStruct);
int parse_case_item(struct data_struct *data, struct case_item *pItem);
int parse_case_clause_recursive(struct data_struct *data, struct case_clause *pStruct);

#endif //INC_42SH_LL_H
