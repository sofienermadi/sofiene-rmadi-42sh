#ifndef INC_42SH_PARSE_H
#define INC_42SH_PARSE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

enum options
{
    EMPTY    = 1 << 0,
    COMMAND  = 1 << 1,
    NORC     = 1 << 2,
    ASTPRINT = 1 << 3,
    VERSION  = 1 << 4,
    NONE     = 1 << 5
};

enum options parse_options(int argc, char **argv, char **command);

#endif //INC_42SH_PARSE_H
