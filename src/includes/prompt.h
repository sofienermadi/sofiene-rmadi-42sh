#ifndef INC_42SH_PROMPT_H
#define INC_42SH_PROMPT_H
#define KGRN  "\x1B[32m"
#define RESET "\x1B[0m"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <readline/readline.h>
#include <readline/history.h>

void loop_prompt();


#endif //INC_42SH_PROMPT_H