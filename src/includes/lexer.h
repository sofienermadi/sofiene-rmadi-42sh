#ifndef INC_42SH_LEXER_H
#define INC_42SH_LEXER_H

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include "ll_parser.h"

char *eat_next_word(struct data_struct *pStruct);
char *check_next_word(struct data_struct *pStruct);
char *check_here_doc(struct data_struct *pStruct);
char check_next_char(struct data_struct *pStruct);
char *eat_comparator(struct data_struct *pStruct);

#endif //INC_42SH_LEXER_H
