#define _GNU_SOURCE

#include "../includes/string_utilities.h"
#include "../includes/file_reader_utilities.h"
#include "../includes/tokenizer.h"

// int main(int argc, char *argv[])
// {
//     printf("%s ,%d\n", argv[0], argc );
//     char *filecontent = get_file_content("tests/forloop.sh");
//     run_tokenizer(filecontent);
//     free(filecontent);
//     return 1;
// }

char *tokenize(char *input, int index, char **tokens, int tokens_counter)
{
    tokens[tokens_counter] = malloc(index + 1);
    strncpy(tokens[tokens_counter] ,input, index);
    input = input + index +1;
    return input;
}

void retrieve_comment(char *input)
{
    while(input && input[0] != '\n' && input[0] != EOF)
        input++;
}

int skip_until(char *input, char *value)
{
    int i = 0;
    while(*(input + i))
    {
        if(strncmp(input+i, value, strlen(value)) == 0)
            break;
        i++;
    }
    return i;
}

int skip_until_op(char *input, char *value)
{
    int i = 0;
    while(*(input + i))
    {
        if(strncmp(input+i, value, strlen(value)) == 0
            && input[i-1] != '\\')
            break;
    }
    return i;
}

char *skip_tab_and_space(char *input)
{
    while(input && (input[0] == '\t' ||input[0] == ' ' ||input[0] == '\n'))
        input++;
    printf("end : skip_tab_and_space. \n");
    return input;
}

char *starts_with_keyword(char *input)
{
    char *words[] = { "if", "then", "else", "elif", "fi", "do", "done",
      "case", "esac", "while", "until", "for"};
    int i = 0;
    while((words[i]))
    {
        char *str = malloc(strlen(words[i])+1);
        str = strcat(str, words[i]);
        strcat(str, " ");   
        if(strncmp(input, str, strlen(str)) == 0)
        {
            free(str);
            return words[i]; 
        }
        i++;
        free(str);
    }
    return NULL;
}

void print_tokens(char *tokens[])
{
    int i = 0;
    printf("{ "  );
    while(tokens[i] != NULL && strcmp(tokens[i], "") != 0)
    {
        printf("*%s* ,", tokens[i]);
        i++;
    }
    printf("} "  );
}

void run_tokenizer(char *input)
{
    int i = 0; 
    int tokens_counter = 0;
    char *tokens[500] ;
    int double_quote = 0;
    double_quote++;
    for (int i = 0; i < 500; ++i)
        tokens[i] = malloc(sizeof(char*));
    while(*(input + i))
    {
        input = skip_tab_and_space(input);
        if(input[i] == '"' )
        {
            i = i + skip_until(input, "\"");    
            input = tokenize(input, i, tokens, tokens_counter);
            tokens_counter++;
            i = 0;
        }
        input = skip_tab_and_space(input);
        if(input[i] == '\'' )
        {
            i = i + skip_until(input, "\'");
            input = tokenize(input, i, tokens, tokens_counter);
            tokens_counter++;
            i = 0;
        }
        if(input[i] == '#' )
        {
            i = i + skip_until(input, "\n");
            input = input + i + 1;
            i = 0;
        }
        if(is_sample_char(input[i]) == 1)
        {
            i = i + skip_until(input, " ");
            input = tokenize(input, i, tokens, tokens_counter);
            tokens_counter++;
            i = 0;
        }
        i = i + skip_until(input, " ");
        input = tokenize(input, i, tokens, tokens_counter);
        tokens_counter++;
        i = 0;
    }
    print_tokens(tokens);
    return ;
}