#ifndef INC_42SH_TOKENIZER_H
#define INC_42SH_TOKENIZER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

char **split(const char *string, const char *delim);
int str_contains(const char *str,const char* substr);
void run_tokenizer(char *input);
char *get_file_content(char* filename);

#endif //INC_42SH_TOKENIZER_H
