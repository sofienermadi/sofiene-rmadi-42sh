#ifndef FILE_READER_UTILITIES
#define FILE_READER_UTILITIES

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/**
* 	function checks if token is alphabetic
*	
*  	 \return char* containing the file content
**/
char *get_file_content(char* filename);

#endif // FILE_READER_UTILITIES