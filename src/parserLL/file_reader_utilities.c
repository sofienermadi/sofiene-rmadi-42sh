#include "../includes/file_reader_utilities.h"

char *get_file_content(char* filename)
{
    FILE *file = fopen(filename, "r+");
    if(!file){
        printf("cannot open file : %s\n", filename);
        exit(0);
    }
    fseek(file, 0, SEEK_END); // seek to end of file
    long size = ftell(file); // get current file pointer
    fseek(file, 0, SEEK_SET);
    char *content = malloc(size + 1);
    fread(content, size, 1, file);
    fclose(file);
    return content; 
}
