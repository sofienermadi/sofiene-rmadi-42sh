#ifndef INC_42SH_HANDLE_H
#define INC_42SH_HANDLE_H

#include "parse.h"
#include "version.h"

int handle_options(enum options opt, char *command);

#endif //INC_42SH_HANDLE_H
