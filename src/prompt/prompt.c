#include "../includes/prompt.h"

void loop_prompt()
{
    char cwd[1024];
    char *command = NULL;
    while (1)
    {
        getcwd(cwd, sizeof(cwd));
        printf("%s%s$", KGRN, cwd);
        printf(" %s", RESET);
        command = readline("");
        printf("%s\n", command);
        if (!strcmp(command, "exit"))
            exit(0);
    }
}