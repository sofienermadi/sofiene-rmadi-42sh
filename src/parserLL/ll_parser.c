#include "../includes/ll_parser.h"
#include "../includes/lexer.h"


static int node_id = 3000;

int parse_input(struct data_struct *data, struct node *root)
{
    int index_save = data->index;

    root->id = node_id++;
    root->input.type = e_input;
    root->input.list.nb_ao = 0;
    root->input.list.nb_op = 0;
    int return_value = parse_list(data, &root->input.list, 1);
    if(return_value == 1)
        data->index = index_save;
    char *next_word = eat_next_word(data);
    if(!strcmp(next_word, "\n"))
        return 0;
    else if(!strcmp(next_word, "\0"))
        return 0;
    return 1;
}

int parse_list(struct data_struct *data, struct list *pNode, int firstRun)
{

    pNode->id = node_id++;
    pNode->type = e_list;
    ++pNode->nb_ao;
    if (firstRun)
        pNode->and_ors = malloc(sizeof(struct and_or));
    else
        pNode->and_ors = realloc(pNode->and_ors, pNode->nb_ao * sizeof(struct and_or));
    //and_or(( '; '| '& ') and_or )* [';'|'&']
    if(parse_and_or(data, &pNode->and_ors[pNode->nb_ao - 1], 1) == 1)
    {
        pNode->nb_ao--;
        return (firstRun == 1);
    }
    //(( '; '| '& ') and_or )* [';'|'&']
    char *next_word = check_next_word(data);
    if((!strcmp(next_word, ";")) || (!strcmp(next_word, "&")))
    {
        ++pNode->nb_op;
        if (firstRun)
            pNode->operators = malloc(sizeof(struct operators));
        else
            pNode->operators = realloc(pNode->operators, pNode->nb_op * sizeof(struct operators));

        pNode->operators->token = strdup(next_word);
        //(( '; '| '& ') and_or )+ [';'|'&'] ou ( '; '| '& ')
        data->index++;
        return parse_list(data, pNode, 0); //and_or(( '; '| '& ') and_or )* [';'|'&']
    }
    return 0;
}

int parse_and_or(struct data_struct *data, struct and_or *pNode, int firstRun)
{
    if (firstRun)
    {
        pNode->id = node_id++;
        pNode->nb_pp = 0;
        pNode->nb_op = 0;
        pNode->type = e_and_or;
    }

    pNode->nb_pp++;

    if (firstRun)
        pNode->pipelines = malloc(sizeof(struct pipeline));
    else
        pNode->pipelines = realloc(pNode->pipelines, pNode->nb_pp * sizeof(struct pipeline));

    //and_or(( '; '| '& ') and_or )* [';'|'&']
    if(parse_pipeline(data, &pNode->pipelines[pNode->nb_pp - 1]) == 1)
    {
        pNode->nb_pp--;
        return 1;
    }
    //(( '; '| '& ') and_or )* [';'|'&']
    char *next_word = check_next_word(data);
    if((!strcmp(next_word, "&&")) || (!strcmp(next_word, "||")))
    {
        ++pNode->nb_op;
        pNode->operators = realloc(pNode->operators, pNode->nb_op * sizeof(struct operators));
        pNode->operators[pNode->nb_op - 1].token = strdup(next_word);
        //(( '; '| '& ') and_or )+ [';'|'&'] ou ( '; '| '& ')
        data->index++;
        while (!strcmp(check_next_word(data), "\n"))
            data->index++;
        return parse_and_or(data, pNode, 0); //d_or(( '; '| '& ') and_or )* [';'|'&']
    }
    return 0;
}