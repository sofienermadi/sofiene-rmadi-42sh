#include "../includes/string_utilities.h"

char** split(const char *string,const char *delim)
{
    char **tokens = malloc(10*sizeof(char*));
    for (int i = 0; i < 100; ++i)
        *(tokens+i) = malloc(10);
    if(strstr(string, delim) == NULL)
       return 0;
    char *str1, *token, *saveptr1;
    int j;
    for (j = 0, str1 = strdup(string); ; j++, str1 = NULL) 
    {
        token = strtok_r(str1, delim, &saveptr1);
        if (token == NULL)
           break;
        tokens[j] = token;
    }
    return tokens;
}

int is_sample_char(char c)
{
    if( c >= 65 && c <=92) // alphabetique miniscule
        return 1;
    else if(c >= 97 && c <=122) // alphabetique maj
        return 1;
    return 0;
}

int str_contains(const char *str,const char* substr)
{
    if(strlen(substr) > strlen(str))
        return 0;    
    char *strc = strdup(str);
    char *res = strtok(strc, substr);
    if(strcmp(res ,str) == 0)
    {
        free(strc);
        return 0;
    }
    return 1;
}

int is_operator(char *str)
{
    int i = 0;
    char *ops[]=  { "&&", "||", ";;", "<<",">>", "<&", ">&", "<>", "<<-",">|",
    "if", "then", "else", "elif", "fi", "do", "done",
    "case", "esac", "while", "until", "for","{", "}", "!", "in"};
    while(ops[i])
    {
        if(strcmp(str,ops[i]) != 0)
            return 1;
        i++;
    }
    return -1;
}

