#ifndef INC_42SH_AST_H
#define INC_42SH_AST_H

#include <stddef.h>
#include <malloc.h>



enum type
{
    e_input,
    e_list,
    e_and_or,
    e_pipeline,
    e_command,
    e_simple_command,
    e_shell_command,
    e_funcdec,
    e_redirection,
    e_prefix,
    e_element,
    e_compound_list,
    e_rule_for,
    e_rule_while,
    e_rule_until,
    e_rule_case,
    e_rule_if,
    e_else_clause,
    e_elif,
    e_do_group,
    e_case_clause,
    e_case_item,
    e_assign_word,
    e_word
};

struct word
{
    enum type type;
    int id;
    char *token;
};

struct assignment_word
{
    enum type type;
    int id;
    char *token;
};

struct compound_list
{
    enum type type;
    int id;
    int nb_aors;
    int nb_ops;
    struct and_or *and_ors;
    struct operators *operators;
};

struct operators
{
    enum type type;
    int id;
    char *token;
};

struct heredoc
{
    enum type type;
    int id;
    char *token;
};

struct ionumber
{
    enum type type;
    int id;
    int token;
};

struct pipeline
{
    enum type type;
    int id;
    int bang;
    int nb_c;
    struct command *commands;
};

struct list
{
    enum type type;
    int id;
    size_t nb_ao;
    size_t nb_op;
    struct and_or *and_ors;
    struct operators *operators;
};

struct and_or
{
    enum type type;
    int id;
    size_t nb_pp;
    size_t nb_op;
    struct pipeline *pipelines;
    struct operators *operators;
};

struct do_group
{
    enum type type;
    int id;
    struct compound_list compound_list;
};
struct case_clause
{
    enum type type;
    int id;
    int nb_cc;
    struct case_item *case_items;
    struct operators *operators;
};
struct rule_case
{
    enum type type;
    int id;
    struct word word;
    struct case_clause case_clause;
};

struct rule_until
{
    enum type type;
    int id;
    struct compound_list compound_list;
    struct do_group do_group;
};

struct rule_for
{
    enum type type;
    int id;
    int nb_words;
    struct word *words;
    struct do_group do_group;
};

struct rule_while
{
    enum type type;
    int id;
    struct compound_list compound_list;
    struct do_group do_group;
};
struct redirection
{
    enum type type;
    int id;
    struct ionumber ionumber;
    struct operators operator;
    union
    {
        struct word word;
        struct heredoc heredoc;
    }rest;
};
struct element
{
    enum type type;
    int id;
    union
    {
        struct word word;
        struct redirection redirection;
    }element_u;
};

struct prefix
{
    enum type type;
    int id;
    union
    {
        struct assignment_word assignment_word;
        struct redirection redirection;
    }prefix_u;
};

struct rule_else
{
    enum type type;
    int id;
    struct compound_list compound_list;
};

typedef struct else_clause else_clause;

struct rule_elif
{
    enum type type;
    int id;
    struct compound_list compound_lists[2];
    struct else_clause *else_clause;
};

struct else_clause
{
    enum type type;
    int id;
    union
    {
        struct rule_else rule_else;
        struct rule_elif rule_elif;
    }rule;
};

struct rule_if
{
    enum type type;
    int id;
    struct compound_list compound_lists[2];
    struct else_clause else_clause;
};

struct shell_command
{
    enum type type;
    int id;
    int par;
    int acc;
    union
    {
        struct compound_list *compound_list;
        struct rule_for *rule_for;
        struct rule_while *rule_while;
        struct rule_until *rule_until;
        struct rule_if *rule_if;
        struct rule_case *rule_case;
    }son;
};

struct funcdec
{
    enum type type;
    int id;
    int function;
    struct word word;
    struct shell_command shell_command;
};
struct simple_command
{
    enum type type;
    int id;
    struct prefix *prefixs;
    struct element *elements;
    int np_ele;
    int np_pre;

};

struct command
{
    int nb_re;
    enum type type;
    int id;
    union
    {
        struct simple_command simple_command;
        struct shell_command shell_command;
        struct funcdec funcdec;
    }commands;

    struct redirection *redirections;
};


struct case_item
{
    enum type type;
    int id;
    int par;
    int nb_words;
    struct word *words;
    struct compound_list compound_list;
};
struct input
{
    enum type type;
    int id;
    struct list list;
};



struct s_node {
    union {
        struct input input;
        struct list list;
        struct and_or and_or;
        struct pipeline pipeline;
        struct command command;
        struct simple_command simple_command;
        struct shell_command shell_command;
        struct funcdec funcdec;
        struct redirection redirection;
        struct prefix prefix;
        struct element element;
        struct compound_list compound_list;
        struct rule_for rule_for;
        struct rule_while rule_while;
        struct rule_until rule_until;
        struct rule_case rule_case;
        struct rule_if rule_if;
        struct else_clause else_clause;
        struct do_group do_group;
        struct case_clause case_clause;
        struct case_item case_item;
    } s_union;
};

struct node
{
    int id;
    struct input input;
};


struct node create_node(enum type type, struct s_node *struct_node);

#endif //INC_42SH_AST_H