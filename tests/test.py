#!/usr/bin/python
import os 
import inspect
import sys
from os import listdir
from os.path import isfile, join
from colorama import init, Fore, Back, Style
from termcolor import colored

def get_output():
	return str(os.popen("echo $?").read())

def get_file_content(file_name):
	f = open(file_name , 'r')
	c = f.read() 
	f.close()
	return c

def get_test_categories():
	x = [name for name in os.listdir("tests") if os.path.isdir("tests/" + name)]
	print "test categories: " 
	print x
	return x

def cprint(msg, foreground = "black", background = "null"):
    fground = foreground.upper()
    bground = background.upper()
    if background != "null":
    	style = getattr(Fore, fground) + getattr(Back, bground)
    else:
    	style = getattr(Fore, fground)
    print(style + msg + Style.RESET_ALL)

def execute_test_category(category):
	if not os.path.isdir("tests/" + category):
		print('No Such category: '	 + category)
		print_usage()
	else: 
		files = get_dir_files("tests/" + category)
		for f in files:
			os.system(join("tests/", category ,f))
	exit(0)

def get_dir_files(dirname):
	files = [f for f in listdir(dirname) if isfile(join(dirname, f))]
	return files

def print_usage():
	print("Usage: ")
	print("		-l and --list : Display the list of test categories.")
	print('		-c <category> and --category <category> : Execute the'
	+ ' test suite on the categories passed in argu-ment only.')
	print("		-s and --sanity Execute the test suite with sanity checks enabled")
	exit(1)

if __name__ == "__main__":
	if len(sys.argv) == 1:
		print_usage()
	if sys.argv[1] == "--list" or sys.argv[1] == "-l":
		get_test_categories()
		exit(0)
	if sys.argv[1] == "--category" or sys.argv[1] == "-c":
		if sys.argv[2] == "":
			print_usage()
		execute_test_category(sys.argv[2])
		exit(0)
	get_file_content("tests/forloop.sh")
	frame = inspect.currentframe()
	cprint("error at line => " + str(frame.f_lineno), "red")
