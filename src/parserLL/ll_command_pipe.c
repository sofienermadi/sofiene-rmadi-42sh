#include "../includes/ll_parser.h"
#include "../includes/lexer.h"

static int node_id = 2000;


static int recursive_pipeline(struct data_struct *data,
                              struct pipeline *pNode)
{
    pNode->id = node_id++;
    if(!strcmp(check_next_word(data), "|"))
    {
        data->index++;
        ++pNode->nb_c;
        pNode->commands = realloc(pNode->commands, pNode->nb_c * sizeof(struct command));
        //and_or(( '; '| '& ') and_or )* [';'|'&']
        pNode->commands[pNode->nb_c - 1].nb_re = 0;
        if(parse_command(data, &pNode->commands[pNode->nb_c - 1]) == 1)
        {
            pNode->nb_c--;
            return 1;
        }
        while (!strcmp(check_next_word(data), "\n"))
            data->index++;
        return recursive_pipeline(data, pNode);
    }
    return 0;
}

int parse_pipeline(struct data_struct *data, struct pipeline *pNode)
{
    pNode->id = node_id++;
    pNode->type = e_pipeline;
    char *next_word = check_next_word(data);
    pNode->bang = 0;
    if(!strcmp(next_word, "!"))
    {
        pNode->bang = 1;
        data->index++;
    }
    pNode->nb_c = 1;
    pNode->commands = malloc(sizeof(struct command));
    pNode->commands[0].nb_re = 0;
    //and_or(( '; '| '& ') and_or )* [';'|'&']
    if(parse_command(data, &pNode->commands[0]) == 1)
    {
        pNode->nb_c--;
        free(pNode->commands);
        return 1;
    }
    return recursive_pipeline(data, pNode);
}

int parse_command(struct data_struct *data, struct command *pCommand)
{
    pCommand->id = node_id++;
    int index_save = data->index;
    if (parse_simple_command(data, &pCommand->commands.simple_command) == 0)
    {
        pCommand->type = e_simple_command;
        return 0;
    }
    data->index = index_save;
    if (parse_shell_command(data, &pCommand->commands.shell_command) == 1)
    {
        data->index = index_save;
        if (parse_funcdec(data, &pCommand->commands.funcdec) == 1)
            return 1;
        pCommand->type = e_funcdec;
    }
    else
        pCommand->type = e_shell_command;
    do
    {
        index_save = data->index;
        ++pCommand->nb_re;
        pCommand->redirections = realloc(pCommand->redirections, pCommand->nb_re * sizeof (struct redirection));
    } while (parse_redirection(data, &pCommand->redirections[pCommand->nb_re - 1]) == 0);
    pCommand->nb_re--;
    data->index = index_save;
    return 0;
}

int parse_simple_command(struct data_struct *data,
                         struct simple_command *pCommand)
{
    pCommand->id = node_id++;
    pCommand->np_pre = 0;
    pCommand->np_ele = 0;
    pCommand->type = e_simple_command;

    int index_save = data->index;

    pCommand->prefixs = malloc(sizeof (struct prefix));

    if (parse_prefix(data, &pCommand->prefixs[0]) == 1)
    {
        free(pCommand->prefixs);
        data->index = index_save;
        pCommand->elements = malloc(++pCommand->np_ele * sizeof (struct element));

        if (parse_element(data, &pCommand->elements[0]) == 1)
        {
            free(pCommand->elements);
            return 1;
        }
        do
        {
            ++pCommand->np_ele;
            index_save = data->index;
            pCommand->elements = realloc(pCommand->elements,
                    pCommand->np_ele * sizeof (struct element));
        }
        while (parse_element(data,
                               &pCommand->elements[pCommand->np_ele - 1]) == 0);
        --pCommand->np_ele;
        data->index = index_save;
    }
    else
    {
        ++pCommand->np_pre;
        //(prefix)* (element)+
        do
        {
            index_save = data->index;
            ++pCommand->np_pre;
            pCommand->prefixs = realloc(pCommand->prefixs,
                    pCommand->np_pre * sizeof (struct prefix));
            //FIXME we need a fonction to recognize a WORD from a ASSIGNMENT WORD
        }
        while (parse_prefix(data,
                             &pCommand->prefixs[pCommand->np_pre - 1]) == 0);
        data->index = index_save;
        --pCommand->np_pre;

        pCommand->elements = malloc(++pCommand->np_ele * sizeof (struct element));

        if(parse_element(data, &pCommand->elements[0]) == 1)
        {
            free(pCommand->elements);
            return 1;
        }
        do
        {
            index_save = data->index;
            ++pCommand->np_ele;
            pCommand->elements = realloc(pCommand->elements,
                    pCommand->np_ele * sizeof (struct element));
            //FIXME we need a fonction to recognize a WORD from a ASSIGNMENT WORD
        }
        while (parse_element(data,
                             &pCommand->elements[pCommand->np_ele - 1]) == 0);
        --pCommand->np_ele;
        data->index = index_save;
    }
    return 0;
}

static int else_shell_command(struct data_struct *data,
                              struct shell_command *pCommand)
{
    int res;
    char next_char = check_next_char(data);
    switch(next_char)
    {
        case 'f' :
            res = parse_rule_for(data, pCommand->son.rule_for);
            break;
        case 'w' :
            res = parse_rule_while(data, pCommand->son.rule_while);
            break;
        case 'u' :
            res = parse_rule_until(data, pCommand->son.rule_until);
            break;
        case 'c' :
            res = parse_rule_case(data, pCommand->son.rule_case);
            break;
        case 'i' :
            res = parse_rule_if(data, pCommand->son.rule_if);
            break;
        default:
            res = 1;
    }
    return res;
}

int parse_shell_command(struct data_struct *data,
                        struct shell_command *pCommand)
{
    pCommand->id = node_id++;
    pCommand->type = e_shell_command;

    if (!strcmp(check_next_word(data), "("))
    {
        data->index++;
        pCommand->par = 1;
        if (parse_compound_list(data,
                                pCommand->son.compound_list) == 1)
            return 1;
        if (strcmp(eat_next_word(data), ")"))
            return 1;
    }
    else if (!strcmp(check_next_word(data), "{"))
    {
        data->index++;
        pCommand->acc = 1;
        if (parse_compound_list(data,
                                pCommand->son.compound_list) == 1)
            return 1;
        if (strcmp(eat_next_word(data), "}"))
            return 1;
    }
    else
        return else_shell_command(data, pCommand);
    return 0;
}

int parse_funcdec(struct data_struct *data, struct funcdec *pCommand)
{
    pCommand->id = node_id++;
    pCommand->type = e_funcdec;
    pCommand->function = 0;
    if(!strcmp(check_next_word(data), "function"))
    {
        pCommand->function = 1;
        data->index++;
    }

    char *word = eat_next_word(data);
    pCommand->word.token = strdup(word);

    if(strcmp(eat_next_word(data), "("))
        return 1;
    if(strcmp(eat_next_word(data), ")"))
        return 1;

    while(!strcmp(check_next_word(data), "\n"))
        data->index++;

    return parse_shell_command(data, &pCommand->shell_command);
}