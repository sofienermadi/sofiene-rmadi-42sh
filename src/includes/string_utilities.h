#ifndef STRING_UTILITIES_H
#define STRING_UTILITIES_H

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/**
* 	function breaks a string into a sequence of zero or more
*       nonempty tokens.
*	
*  	 \return char**
**/
char** split(const char *string,const char *delim);
/**
* 	function checks if token is alphabetic
*	
*  	 \return 1 if true, 0 else
**/
int is_sample_char(char c);

int str_contains(const char *str,const char* substr);
/**
* 	function checks if token is operator or a reserved 
*	or a reserved keyword
*	
*  	 \return 1 if true, 0 else
**/
int is_operator(char *str);
char *starts_with_keyword(char *input);

#endif //STRING_UTILITIES_H