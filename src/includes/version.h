//
// Created by legarr_m on 5/20/17.
//

#ifndef INC_42SH_VERSION_H
#define INC_42SH_VERSION_H

#define CURRENT_VERSION "0.5"

#include <stdio.h>

void print_version();

#endif //INC_42SH_VERSION_H
