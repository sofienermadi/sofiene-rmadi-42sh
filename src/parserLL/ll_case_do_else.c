#include "../includes/ll_parser.h"
#include "../includes/lexer.h"

static int node_id = 1000;


int parse_else_clause(struct data_struct *data,

                     struct else_clause *else_clause)
{
    else_clause->id = node_id++;
    else_clause->type = e_else_clause;
    char *next_word = eat_next_word(data);
    if(!strcmp(next_word, "else"))
    {
        else_clause->type = e_else_clause;
        return parse_compound_list(data, &else_clause->rule.rule_else.compound_list);
    }
    else if(!strcmp(next_word, "elif"))
    {
        else_clause->type = e_elif;
        if(parse_compound_list(data, &else_clause->rule.rule_elif.compound_lists[0]) == 1)
            return 1;
        if(strcmp(eat_next_word(data), "then"))
            return 1;
        if(parse_compound_list(data, &else_clause->rule.rule_elif.compound_lists[1]) == 1)
            return 1;
        int index_save = data->index;
        if(parse_else_clause(data, else_clause->rule.rule_elif.else_clause) == 1)
            data->index = index_save;
        return 0;
    }
    return 1;
}
int parse_do_group(struct data_struct *data, struct do_group *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_do_group;
    char *tok = eat_next_word(data);
    if(strcmp(tok, "do"))
        return 1;
    if(parse_compound_list(data, &pStruct->compound_list) == 1)
        return 1;
    if(strcmp(tok, "done"))
        return 1;
    return 0;
}
int parse_case_clause(struct data_struct *data, struct case_clause *pStruct)
{
    pStruct->id = node_id++;
    pStruct->type = e_case_clause;
    pStruct->nb_cc = 1;
    pStruct->case_items = malloc(sizeof (struct case_item));
    if (parse_case_item(data, &pStruct->case_items[0]) == 1)
    {
        free(pStruct->case_items);
        pStruct->nb_cc = 0;
        return 1;
    }
    return parse_case_clause_recursive(data, pStruct);
}
int parse_case_clause_recursive(struct data_struct *data,
                                struct case_clause *pStruct)
{
    //';;' ('\n')* case_item (';;' ('\n')* case_item)* [;;] ('\n')*
    //  ou
    //';;' ('\n')*
    //  ou
    //('\n')+
    //  ou
    // _e_o_e
    char *next_word = check_next_word(data);
    if(!strcmp(next_word, "\n"))//('\n')*
    {
        do
            data->index++;
        while (!strcmp(check_next_word(data), "\n"));
        return 0;
    }
    //';;' ('\n')* case_item (';;' ('\n')* case_item)* [;;] ('\n')*
    //  ou
    //';;' ('\n')*
    //  ou
    // _e_o_e
    if(!strcmp(next_word, ";;"))
    {
        //';;' ('\n')* case_item (';;' ('\n')* case_item)* [;;] ('\n')*
        //  ou
        //';;' ('\n')*
        data->index++;
        while (!strcmp(check_next_word(data), "\n"))
            data->index++;
        // case_item (';;' ('\n')* case_item)* [;;] ('\n')*
        // ou
        // _e_o_e
        int index_save = data->index;
        ++pStruct->nb_cc;
        pStruct = realloc(pStruct->case_items, pStruct->nb_cc * sizeof(struct case_item));
        if (parse_case_item(data, &pStruct->case_items[pStruct->nb_cc - 1]) == 1)
        {
            pStruct->nb_cc--;
            data->index = index_save;
            return 0;
        }
        // case_item (';;' ('\n')* case_item)* [;;] ('\n')*
        return parse_case_clause(data, pStruct);
    }
    // _e_o_e
    return 0;
}
int parse_case_item(struct data_struct *data, struct case_item *pItem)
{
    pItem->type = e_case_item;
    pItem->id = node_id++;
    char *next_word = check_next_word(data);
    pItem->par = 0;
    if(!strcmp(next_word, "("))
    {
        pItem->par = 1;
        data->index++;
    }
    char *word = eat_next_word(data);
    pItem->words = malloc(sizeof(struct word));
    strcpy(pItem->words[0].token, word);
    //('|' word)* ')' ('\n')* [ compound_list ]
    // ')' ('\n')* [ compound_list ]
    while(!strcmp(check_next_word(data), "|"))
    {
        //('|' word)+ ')' ('\n')* [ compound_list ]
        data->index++;
        word = eat_next_word(data);
        pItem->nb_words++;
        pItem->words = realloc(pItem->words, pItem->nb_words * sizeof(struct word));
        pItem->words[pItem->nb_words - 1].token = strdup(word);
    }
    if(strcmp(eat_next_word(data), ")"))// ')' ('\n')* [ compound_list ]
        return 1;
    while (!strcmp(check_next_word(data), "\n"))
        data->index++;
    int index_save = data->index;
    if(parse_compound_list(data, &pItem->compound_list) == 1)
        data->index = index_save;
    return 0;
}