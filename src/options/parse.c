#include "../includes/parse.h"

enum options parse_options(int argc, char **argv, char **command)
{
    int opt;
    enum options correct = 0;
    static struct option long_options[] =
    {
      {"version", no_argument, 0, 'v'},
      {"norc", no_argument,    0, 'N'},
      {"ast-print", no_argument, 0, 'A'},
      {0, 0, 0, 0}
    };
    int option_index = 0;
    opt = getopt_long(argc, argv, "c:v", long_options, &option_index);
    switch (opt) {
        case 'c':
            if (!(correct & COMMAND))
                *command = optarg;
            correct |= COMMAND;
            break;
        case 'v':
            correct |= VERSION;
            break;
        case 'N':
            correct |= NORC;
            break;
        case 'A':
            correct |= ASTPRINT;
            break;
        default:
            return NONE; // TODO CHANGE THIS ERRATIC BEHAVIOUR
    }
    return correct;
}